from setuptools import find_packages, setup

setup (
    name = 'flaskr',
    description='A blog application',
    author='Joao Victor',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'flask',
    ],
)